////  HeaderView.swift
//  SwiftUI2_AnimatedHeader
//
//  Created on 15/02/2021.
//  
//

import SwiftUI

struct HeaderView: View {
    
    @Environment(\.colorScheme) var scheme
    
    var body: some View {
        HStack(spacing: 20, content: {
            
            Image("youtube")
                .resizable()
                .scaledToFit()
                .frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
            Text("YT")
                .fontWeight(.bold)
                .font(.title2)
                .foregroundColor(.primary)
                .offset(x: -10)
            
            Spacer(minLength: 0)
            
            
            Button(action: {}, label: {
                Image(systemName: "display")
                    .font(.title2)
                    .foregroundColor(.primary)
            })
            
            Button(action: {}, label: {
                Image(systemName: "bell")
                    .font(.title2)
                    .foregroundColor(.primary)
            })
            
            Button(action: {}, label: {
                Image(systemName: "magnifyingglass")
                    .font(.title2)
                    .foregroundColor(.primary)
            })
            
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                Image("profile")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 30, height: 30)
                    .clipShape(Circle())
            })
            
        }) //: HSpacing
        
        .padding(.horizontal)
        .padding(.vertical,8)
        .background((scheme == .dark ? Color.black : Color.white).ignoresSafeArea(.all, edges: .top))
        
        .overlay(
            Divider(),alignment: .bottom
        )
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView()
            .previewDevice("iPhone 11 Pro")
            
    }
}

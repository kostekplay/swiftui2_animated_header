////  HomeView.swift
//  SwiftUI2_AnimatedHeader
//
//  Created on 15/02/2021.
//  
//

import SwiftUI

struct HomeView: View {
    
    var rect = UIScreen.main.bounds
    var edges = UIApplication.shared.windows.first?.safeAreaInsets
    
    @StateObject var headerData = HeaderViewModel()
    
    init() {
        UIScrollView.appearance().bounces = false
    }
    
    var body: some View {
        
        ZStack(alignment: .top, content: {
            
            HeaderView()
                .zIndex(1)
                .offset(y: headerData.headerOffset)
            
            ScrollView(.vertical, showsIndicators: false, content: {
                
                VStack(spacing: 15, content: {
                    
                    ForEach(1...5, id: \.self) { index in
                        
                        Image("thumb1")
                            .resizable()
                            .scaledToFill()
                            .frame(width: rect.width - 30, height: 250)
                        
                        HStack(spacing: 10, content: {
                            Image("profile")
                                .resizable()
                                .scaledToFill()
                                .frame(width: 35, height: 35)
                                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                            
                            VStack(alignment: .leading, spacing: 4, content: {
                                
                                Text("New Sony Camera Unboxng And Review")
                                    .fontWeight(.bold)
                                    .foregroundColor(.primary)
                                    .lineLimit(2)
                                    .multilineTextAlignment(.leading)
                                
                                Text("iJustine . 4 hours ago")
                                    .font(.caption)
                                    .foregroundColor(.gray)
                                
                            }) //: VStack
                        }) //:HStack
                        .frame(alignment: .leading)
                        .padding(.horizontal)
                        .padding(.bottom, 15)
                    } //: ForEach
                }) //: VStack
                .padding(.top,75)
                .overlay(
                    GeometryReader { proxy -> Color in
                        let minY = proxy.frame(in: .global).minY
                        DispatchQueue.main.async {
                            // storing intial MinY value...
                            if headerData.startMinY == 0 {
                                headerData.startMinY = minY
                            }
                            
                            // getting exact offset value by subtracting current from start...
                            let offset = headerData.startMinY - minY
                            
                            // getting scroll direction....
                            if offset > headerData.offset{

                                // if top hiding Header View....
                                // same clearing bottom offset...
                                headerData.bottomScrollOffset = 0
                                if headerData.topScrollOffset == 0{
                                    // Storing Intially to subtract the maxOffset...
                                    headerData.topScrollOffset = offset
                                }
                                let progress = (headerData.topScrollOffset + getMaxOffset()) - offset
                                // all conditions were going to use ternary operator
                                // because if we use if else while swiping fast it ignore some conditions....
                                let offsetCondition = (headerData.topScrollOffset + getMaxOffset()) >= getMaxOffset() && getMaxOffset() - progress <= getMaxOffset()
                                let headerOffset = offsetCondition ? -(getMaxOffset() - progress) : -getMaxOffset()
                                headerData.headerOffset = headerOffset
                            }
                            
                            if offset < headerData.offset{
                                // if bottom revelaing header view....
                                // clearing topscrollvalue and setting botttom...
                                headerData.topScrollOffset = 0
                                
                                if headerData.bottomScrollOffset == 0{
                                    
                                    headerData.bottomScrollOffset = offset
                                }
                                
                                // Moving if little bit of screen is swiped down...
                                // for eg 40 offset...
                                
                                withAnimation(.easeOut(duration: 0.2)){
                                    
                                    let headerOffset = headerData.headerOffset
                                    
                                    let conditions = headerData.bottomScrollOffset > offset + 40
                                    
                                    headerData.headerOffset = conditions ? 0 : (headerOffset != -getMaxOffset() ? 0 : headerOffset)
                                }
                            }
                            headerData.offset = offset
                        }
                        return Color.clear
                    } //: GeometryReader
                    .frame(height: 0),alignment: .top
                ) //: overlay
            }) //: ScrollView
        }) //: ZStack
    }
    
    func getMaxOffset()->CGFloat{
        return headerData.startMinY + (edges?.top ?? 0) + 10
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

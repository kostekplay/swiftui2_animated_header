////  HeaderViewModel.swift
//  SwiftUI2_AnimatedHeader
//
//  Created on 15/02/2021.
//  
//

import SwiftUI

class HeaderViewModel: ObservableObject {
    
    @Published var startMinY : CGFloat = 0
    @Published var offset: CGFloat = 0
    @Published var headerOffset: CGFloat = 0
    
    @Published var topScrollOffset: CGFloat = 0
    @Published var bottomScrollOffset: CGFloat = 0
    
}

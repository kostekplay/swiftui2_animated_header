////  SwiftUI2_AnimatedHeaderApp.swift
//  SwiftUI2_AnimatedHeader
//
//  Created on 15/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_AnimatedHeaderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

////  ContentView.swift
//  SwiftUI2_AnimatedHeader
//
//  Created on 15/02/2021.
//  
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
